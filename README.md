# Holidaze

## About
This application is a student project simulating a accommodation hosting and booking website, named 'Holidaze'.

### Login
- username: admin
- password: admin

## Project setup
- Have Node installed
- run command:
`
npm install
`

### Development
`
npm run serve
`

### Production
`
npm run build
`