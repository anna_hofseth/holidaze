
import JQuery from 'jquery';
const $ = JQuery;

export default {
    install (Vue){
        Vue.mixin({
            data(){
                return{
                    monthsShort: [ "jan", "feb", "march", "april", "may", "june", 
                                   "july", "aug", "sept", "oct", "nov", "dec"]
                }
            },
            methods: {
                getDateObject(dateString){
                    //if (typeof dateString != String) return "";

                    let isVfc = dateString.includes("/");
        
                    if (isVfc){
                        let date = dateString.split('/');
                        return new Date([date[1], date[0], date[2]].join(' '));
                    }
        
                    else{
                        let date = dateString.split('-');
                        return new Date([date[1], date[2], date[0]].join(' '));
                    }
                },
                dateObjectToString(dateObj, vfcFormat = false){
                    let day = this.twoDigits(dateObj.getDate());
                    let month = this.twoDigits(dateObj.getMonth() + 1); //let month = dateObj.getMonth();
                    let year = dateObj.getFullYear();
        
                    if (vfcFormat)
                        return [day, month, year].join('/');
        
                    else
                        return [year, month, day].join('-');
                },
                getAllDates(from, to){
                    let dates = [];
        
                    let current = from;
                    dates.push(from);
                    while(current < to){
                        let addDate = new Date(current);
                        dates.push(addDate);
                        addDate.setDate(addDate.getDate() + 1);
                        current = addDate;
                    }
        
                    return dates;
                },
                getDateSummary(date, includeYear = false){
                    let res = [];
                    res.push(this.monthsShort[date.getMonth()]);
                    res.push(date.getDate() + ".");
                    if (includeYear) 
                        res.push(date.getFullYear());

                    return res.join(' ');
                },
                twoDigits(value){
                    return ('0' + value).slice(-2);
                },
                fetchDataFromUrl(url, callAction){
                    fetch(url, {
                        mode : "cors",
                        method : "GET",
                        headers : {
                            'Content-Type' : 'application/json'
                    }})
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(result) {
                        callAction(result);
                    });
                },
                getEstablishmentsData(callAction){
                    this.fetchDataFromUrl("./data/establishments.json", callAction);
                },
                getBookingData(callAction){
                    this.fetchDataFromUrl("./data/enquiries.json", callAction);
                },
                getEstablishmentsWithBookingData(callAction){
                    const app = this;

                    this.getEstablishmentsData(function(establishments){
                        for(let est of establishments){
                            est.bookingData = [];
                        }           
                        app.getBookingData(function(bookings){
                            // Combining the two json objects. Algorithm is not optimal
                            for(let b = 0; b < bookings.length; b++){
                                let bk = bookings[b];
                                for(let e = 0; e < establishments.length; e++){
                                    let est = establishments[e];
                                    if (est.establishmentName === bk.establishment){
                                        establishments[e].bookingData.push(bk);
                                    }
                                }
                            }
                        });

                        callAction(establishments);
                    })
                },
                validateEmail(email){
                    return (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]{1,}$/.test(email));
                },
                initMapOverrideScrollAndZoom(source){
                    const app = this;

                    /*source.on('mousedown', function () {
                        app.setBrowserScrollAndZoomActive(false);
                    });
                    source.on('mouseup', function () {
                        app.setBrowserScrollAndZoomActive(true);
                    });*/
                },
                setBrowserZoomActive(active){
                    $('meta[name=viewport]').attr('content', active
                        ? "width=device-width,initial-scale=1.0"
                        : "width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0");
                
                },
                setBrowserScrollActive(active){
                    $('body').css("overscroll-behavior-y", active ? "auto" : "contain");
                }
            }
        })
    }
}