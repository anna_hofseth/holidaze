import Vue from 'vue'
import App from './App.vue'
import Main from './Main.vue'
import VueRouter from 'vue-router'
import VueMq from 'vue-mq'
import utils from './utils.js'
Vue.use(VueRouter)
Vue.use(utils)

import Login from './pages/Login.vue'
import About from './pages/About.vue'
import Contact from './pages/Contact.vue'

import Home from './pages/Home.vue'
import Establishment from './pages/Establishment.vue'
import AdminDashboard from './pages/AdminDashboard.vue'
import Listing from './pages/Listing.vue'

Vue.config.productionTip = false

Vue.use(VueMq, {
    breakpoints: {
    mobile: 900,
    desktop: Infinity
  }
});

const router = new VueRouter({
  routes: [
    {
      path: "/",
      redirect: "home",
      component: Main,
      children: [
        {
          name: "Home",
          path: "home",
          component: Home
        },
        {
          name: "About",
          path: "about",
          component: About
        },
        {
          name: "Contact",
          path: "contact",
          component: Contact
        },
        {
          name: "Login",
          path: "login",
          component: Login
        },
        {
          name: "Establishment",
          path: "establishment/:id",
          component: Establishment,
          props: true
        },
        {
          name: "AdminDashboard",
          path: "admin-dashboard",
          component: AdminDashboard
        },
        {
          name: "Listing",
          path: "admin/listing/:id?",
          component: Listing,
          props: true
        }
      ]
    }
  ]
});

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
